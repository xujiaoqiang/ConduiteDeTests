package test;

public class TestMain {
	public static void main(String[] args) {
		TestPile pile = new TestPile();
		TestObserver observerVBP = new TestViewBottomPile();
		TestObserver observerVTP = new TestViewTopPile();
		pile.test_addObserver(observerVBP);
		pile.test_addObserver(observerVTP);
		TestKbdInputPile kbd = new TestKbdInputPile();
		for (int i = 0; i < 5; i++) {
			kbd.test_actionCommande(pile, 10 + i);
		}
	}
}