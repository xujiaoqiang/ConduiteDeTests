package test;

public class TestViewTopPile implements TestObserver {

	TestPile pile;

	@Override
	public void test_update(TestObservable o, Object arg) {
	}

	@Override
	public void stub_update(TestObservable o, Object arg) {
		pile = (TestPile) o;
		int indexMax = pile.list.size() - 1;
		System.out.println("Le sommet : " + String.valueOf(pile.list.get(indexMax)));
	}
}
