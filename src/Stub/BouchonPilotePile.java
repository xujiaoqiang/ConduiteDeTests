package Stub;

import java.util.ArrayList;

public class BouchonPilotePile {

	private static ArrayList<Integer> list = new ArrayList<Integer>();

	public static ArrayList<Integer> getList() {
		return list;
	}

	public void push(int entier) {
		this.list.add(entier);
		BouchonVBP vbp = new BouchonVBP();
		vbp.update();
		System.out.println(" ");
		BouchonVTP vtp = new BouchonVTP();
		vtp.update();
	}
}
